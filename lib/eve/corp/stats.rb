require 'eve/corp/stats/version'
require 'ruby-esi'
require 'thor'


module Eve
  module Corp
    module Stats
      class CLI < Thor
        CORP_ID = 98652587
        WALLET_DIVISION = 1

        def initialize(args = nil, options = nil, config = nil)
          super args, options, config
          @corp_api = ESI::CorporationApi.new(api_client)
          @wallet_api = ESI::WalletApi.new(api_client)
          @character_api = ESI::CharacterApi.new(api_client)
          @universe_api = ESI::UniverseApi.new(api_client)
        end

        desc 'bounty_taxes', 'Displays the amount of bounty taxes each player paid'
        def bounty_taxes
          tax_by_character = {}
          begin
            entries = get_all_journal_entries_between(DateTime.parse('2020-08-01T00:00:00+00:00'), DateTime.parse('2020-09-01T00:00:00+00:00'))
            entries.group_by(&:ref_type).map{ |x, y| [y.inject(0) { |sum, i| sum + i.amount }, x] }
            entries.each do |entry|
              char_id = entry.first_party_id
              char_id = entry.second_party_id unless entry.second_party_id.nil?
              if tax_by_character[char_id].nil?
                tax_by_character[char_id] = { amount: 0, missions: 0 }
              end
              unless entry.tax.nil?
                tax_by_character[char_id][:amount] += entry.amount
              end
              if entry.ref_type == 'agent_mission_reward'
                tax_by_character[char_id][:missions] += 1
              end
            end
            add_character_names(tax_by_character)
            output_bounty_taxes(tax_by_character)
            output_mission_count(tax_by_character)
          rescue ESI::ApiError => e
            puts e.code
            puts e.response_headers
            puts e.response_body
          end
        end

        private

        def add_character_names(data_arr)
          ids = data_arr .map { |k, _v| k }
          names = lookup_char_names(ids)
          data_arr.each do |k, v|
            v[:name] = lookup_for_id(names, k)
          end
        end

        def lookup_for_id(names, id)
          names.each do |name_entry|
            return name_entry.name if id == name_entry.id
          end
          "unknown id: #{id}"
        end

        def output_bounty_taxes(char_stats_id)
          puts '########################'
          puts 'TAX'
          puts '########################'
          char_stats_id = char_stats_id.sort_by { |_k, v| v[:amount] }.reverse
          char_stats_id.each do |char_id, value|
            puts "#{value[:name]}, #{value[:amount]}"
          end
        end

        def output_mission_count(char_stats_id)
          puts '########################'
          puts 'MISSIONS'
          puts '########################'
          char_stats_id = char_stats_id.sort_by { |_k, v| v[:missions] }.reverse
          char_stats_id.each do |char_id, value|
            puts "#{value[:name]}, Missions: #{value[:missions]}"
          end
        end

        def lookup_char_names(character_ids)
          @universe_api.post_universe_names(character_ids, opts)
        end

        def get_all_journal_entries_between(start_date, end_date)
          page = 1
          results = []
          data, total_pages = wallet_journal(page)
          while !data.nil? && page < total_pages
            page += 1
            data, = wallet_journal(page)
            results += filter_journal_entries(data, start_date, end_date)
          end
          results
        end

        # @param [DateTime] start_date
        # @param [DateTime] end_date
        def filter_journal_entries(data, start_date, end_date)
          results = []
          data.each do |entry|
            results << entry if start_date < entry.date && end_date > entry.date
          end
          results
        end

        def wallet_journal(page)
          data, _status_code, headers = @wallet_api.get_corporations_corporation_id_wallets_division_journal_with_http_info(
              CORP_ID, WALLET_DIVISION, opts(page))
          [data, headers['x-pages'].to_i]
        end

        def opts(page = 1)
          {
            datasource: 'tranquility', # String | The server name you would like data from
            user_agent: 'eve-corp-stats', # String | Client identifier, takes precedence over headers
            x_user_agent: 'eve-corp-stats', # String | Client identifier, takes precedence over User-Agent
            page: page
          }
        end

        def api_client
          config = ESI::Configuration.new
          config.access_token = auth_token
          ESI::ApiClient.new(config)
        end

        def auth_token
          File.read('.token')
        end

      end
    end
  end
end
Eve::Corp::Stats::CLI.start(ARGV)