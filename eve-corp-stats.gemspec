require_relative 'lib/eve/corp/stats/version'

Gem::Specification.new do |spec|
  spec.name          = 'eve-corp-stats'
  spec.version       = Eve::Corp::Stats::VERSION
  spec.authors       = ['Kaspar Bach Pedersen']
  spec.email         = ['kasparp@me.com']

  spec.summary       = %q(Eve online corp member statistics utility)
  spec.description   = %q(Eve online corp member statistics utility)
  spec.homepage      = 'https://bitbucket.org/kasparp/eve-corp-stats/'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/kasparp/eve-corp-stats/'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/kasparp/eve-corp-stats/src/master/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
